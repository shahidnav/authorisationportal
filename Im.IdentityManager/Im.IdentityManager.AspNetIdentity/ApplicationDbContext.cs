﻿using System.Configuration;
using AspNet.Identity.EntityFramework.Multitenant;

namespace Im.IdentityManager.AspNetIdentity
{
    public class ApplicationDbContext : MultitenantIdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString)
        { }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }
    }
}