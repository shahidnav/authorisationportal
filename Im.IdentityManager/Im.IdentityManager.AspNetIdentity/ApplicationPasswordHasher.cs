﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using DBTek.Crypto;
using Microsoft.AspNet.Identity;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace Im.IdentityManager.AspNetIdentity
{
    public class ApplicationPasswordHasher : PasswordHasher
    {
        private readonly IEnumerable<Func<string, string, bool>> _legacyPasswordVerifiers;

        public ApplicationPasswordHasher()
        {
            _legacyPasswordVerifiers = new Collection<Func<string, string, bool>>
            {
                Sha512Hash,
                UnsaltedString,
                SaltedString,
                MD5Hash
            };
        }

        public override PasswordVerificationResult VerifyHashedPassword(string hashedPassword, string providedPassword)
        {
            if (base.VerifyHashedPassword(hashedPassword, providedPassword) == PasswordVerificationResult.Success)
                return PasswordVerificationResult.Success;

            if (_legacyPasswordVerifiers.Any(passworVerifier => passworVerifier(hashedPassword, providedPassword)))
            {
                return PasswordVerificationResult.SuccessRehashNeeded;
            }

            return PasswordVerificationResult.Failed;
        }

        private bool Sha512Hash(string hashedPassword, string providedPassword)
        {
            var encoder = new UTF8Encoding();
            var sha512Hasher = new SHA512Managed();

            var hashedDataBytes = sha512Hasher.ComputeHash(encoder.GetBytes(providedPassword));

            return ByteArrayToString(hashedDataBytes) == hashedPassword;
        }

        private bool UnsaltedString(string hashedPassword, string providedPassword)
        {
            var valueToHash = providedPassword;
            var hashProvider = new MD5CryptoServiceProvider();

            var userData = Encoding.UTF8.GetBytes(valueToHash);
            var md5 = hashProvider.ComputeHash(userData);

            return md5.Aggregate(string.Empty, (current, byteItem) => current + byteItem.ToString("x2")) == hashedPassword;

        }

        private bool MD5Hash(string password, string hashedPassword)
        {
            MD5CryptoServiceProvider hashProvider = new MD5CryptoServiceProvider();

            byte[] userData = System.Text.Encoding.ASCII.GetBytes(password);

            byte[] md5 = hashProvider.ComputeHash(userData);

            StringBuilder builder = new StringBuilder();

            foreach (byte md5Byte in md5)
            {
                int val = ((int)md5Byte) & 0xff;
                if (val < 16)
                {
                    builder.Append("0");
                }
                builder.Append(String.Format("{0:X}", val));
            }

            return builder.ToString().ToLower() == hashedPassword;
        }

        private bool SaltedString(string hashedPassword, string providedPassword)
        {
            var salt = string.Empty;

            if (!string.IsNullOrEmpty(providedPassword))
            {
                if (providedPassword.Substring(0, 3) == "$1$")
                {
                    salt = providedPassword.Substring(3, 8);
                }
            }

            if (salt == string.Empty)
            {
                return false;
            }

            var unixCrypt = new UnixCrypt();
            return unixCrypt.HashString(providedPassword, salt, UnixCryptTypes.MD5) == hashedPassword;
        }

        private static string ByteArrayToString(byte[] inputArray)
        {
            var output = new StringBuilder("");
            foreach (byte b in inputArray)
            {
                output.Append(b.ToString("X2"));
            }

            return output.ToString();
        }
    }
}