﻿using System;
using Microsoft.Owin.Hosting;

namespace Im.IdentityManager.IdentityServer3
{
    class Program
    {
        static void Main(string[] args)
        {
            using (WebApp.Start<Startup>("http://localhost:5000"))
            {
                Console.WriteLine("server running...");
                Console.ReadLine();
            }
        }
    }
}
