﻿using System.Collections.Generic;
using IdentityServer3.Core.Models;

namespace Im.IdentityManager.IdentityServer.Models
{
    public static class Clients
    {
        public static IEnumerable<Client> Get()
        {
            return new[]
            {
                new Client
                {
                    Enabled = true,
                    ClientName = "Gardeners world",
                    LogoUri = "http://www.beachbumuk.com/wp-content/uploads/2013/05/logo_bg.png",
                    LogoutUri = "https://localhost:44301/",
                    LogoutSessionRequired = true,
                    ClientId = "mvc1",
                    Flow = Flows.Implicit,

                    ClientUri = "https://localhost:44301/",

                    RedirectUris = new List<string> { "https://localhost:44301/" },

                    AllowAccessToAllScopes = true,
                    
                    PostLogoutRedirectUris = new List<string> { "https://localhost:44301/" }
                },
                new Client
                {
                    Enabled = true,
                    ClientName = "Made for mums",
                    LogoUri = "https://pbs.twimg.com/profile_images/616235633200205824/q6dgmUQW.png",
                    ClientId = "mvc2",
                    Flow = Flows.Implicit,

                    RedirectUris = new List<string>
                    {
                        "https://localhost:44301/"
                    },

                    AllowAccessToAllScopes = true
                }
            };
        }
    }
}