﻿using System;
using System.Data.Entity;
using System.Security.Claims;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using AspNet.Identity.EntityFramework.Multitenant;
using IdentityServer3.Core;
using IdentityServer3.Core.Configuration;
using IdentityServer3.Core.Models;
using IdentityServer3.Core.Services;
using IdentityServer3.Core.Services.Default;
using Im.IdentityManager.AspNetIdentity;
using Im.IdentityManager.IdentityServer;
using Im.IdentityManager.IdentityServer.Models;
using Microsoft.IdentityModel.Protocols;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.OpenIdConnect;
using Owin;
using AuthenticationOptions = IdentityServer3.Core.Configuration.AuthenticationOptions;

[assembly: OwinStartup(typeof(Startup))]

namespace Im.IdentityManager.IdentityServer
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            // Identity Server setup
            app.Map("/identity", idsrvApp =>
            {
                var factory = new IdentityServerServiceFactory()
                    .UseInMemoryClients(Clients.Get())
                    .UseInMemoryScopes(StandardScopes.All);

                factory.Register(new Registration<DbContext>(typeof(ApplicationDbContext)));
                factory.Register(new Registration<MultitenantUserStore<ApplicationUser>>());
                factory.Register(new Registration<MultitenantUserManager>());

                factory.UserService = new Registration<IUserService, UserService>();

                factory.Register(new Registration<DefaultViewServiceOptions>());
                factory.Register(new Registration<IViewLoader, FileSystemWithEmbeddedFallbackViewLoader>());
                factory.ViewService = new Registration<IViewService>(typeof(WhiteLabelViewService));

                //factory.ConfigureDefaultViewService(new DefaultViewServiceOptions
                //{

                //});

                idsrvApp.UseIdentityServer(new IdentityServerOptions
                {
                    SiteName = "Embedded IdentityServer",
                    SigningCertificate = LoadCertificate(),
                    
                    Factory = factory,

                    AuthenticationOptions = new AuthenticationOptions
                    {
                        EnableSignOutPrompt = false,
                        EnablePostSignOutAutoRedirect = true
                    }
                });
            });

            // Identity Client setup
            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationType = "Cookies"
            });
            
            app.UseOpenIdConnectAuthentication(new OpenIdConnectAuthenticationOptions
            {
                Authority = "https://localhost:44301/identity",
                ClientId = "mvc1",
                RedirectUri = "https://localhost:44301/",
                ResponseType = "id_token",
                PostLogoutRedirectUri = "https://localhost:44301/",
                SignInAsAuthenticationType = "Cookies",

                Notifications = new OpenIdConnectAuthenticationNotifications
                {
                    SecurityTokenValidated = n =>
                    {
                        var nid = new ClaimsIdentity(
                            n.AuthenticationTicket.Identity.AuthenticationType,
                            Constants.ClaimTypes.GivenName,
                            Constants.ClaimTypes.Role);

                        // keep the id_token for logout
                        nid.AddClaim(new Claim("id_token", n.ProtocolMessage.IdToken));

                        n.AuthenticationTicket = new AuthenticationTicket(
                            nid,
                            n.AuthenticationTicket.Properties);

                        return Task.FromResult(0);
                    },

                    RedirectToIdentityProvider = n =>
                    {
                        if (n.ProtocolMessage.RequestType == OpenIdConnectRequestType.AuthenticationRequest)
                        {
                            n.ProtocolMessage.AcrValues = "tenant:firstTenant";
                        }

                        if (n.ProtocolMessage.RequestType == OpenIdConnectRequestType.LogoutRequest)
                        {
                            var idTokenHint = n.OwinContext.Authentication.User.FindFirst("id_token");

                            if (idTokenHint != null)
                            {
                                n.ProtocolMessage.IdTokenHint = idTokenHint.Value;
                            }
                        }

                        return Task.FromResult(0);
                    },
                }
            });
        }

        X509Certificate2 LoadCertificate()
        {
            var certPath = $@"{AppDomain.CurrentDomain.BaseDirectory}bin\idsrv3test.pfx";

            return new X509Certificate2(certPath, "idsrv3test");
        }
    }
}
