using System.Threading.Tasks;
using IdentityServer3.Core.Models;
using Im.IdentityManager.AspNetIdentity;
using Im.IdentityManager.IdentityServer.App_Packages.IdentityServer3.AspNetIdentity;

namespace Im.IdentityManager.IdentityServer
{
    public class UserService : AspNetIdentityUserService<ApplicationUser, string>
    {
        public MultitenantUserManager MultitenantUserManager => userManager as MultitenantUserManager;

        public UserService(MultitenantUserManager userMgr)
            : base(userMgr)
        {
        }

        public override async Task AuthenticateExternalAsync(ExternalAuthenticationContext ctx)
        {
            MultitenantUserManager.TenantId = ctx.SignInMessage.Tenant;

            await base.AuthenticateExternalAsync(ctx);
        }

        public override async Task AuthenticateLocalAsync(LocalAuthenticationContext ctx)
        {
            MultitenantUserManager.TenantId = ctx.SignInMessage.Tenant;

            await base.AuthenticateLocalAsync(ctx);
        }
    }
}