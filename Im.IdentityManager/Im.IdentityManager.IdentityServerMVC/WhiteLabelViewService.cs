using System.IO;
using System.Threading.Tasks;
using IdentityServer3.Core.Models;
using IdentityServer3.Core.Services.Default;
using IdentityServer3.Core.ViewModels;

namespace Im.IdentityManager.IdentityServer
{
    public class WhiteLabelViewService : DefaultViewService
    {
        public WhiteLabelViewService(DefaultViewServiceOptions config, IViewLoader viewLoader)
            : base(config, viewLoader)
        { }

        public override Task<Stream> LoggedOut(LoggedOutViewModel model, SignOutMessage message)
        {
            if (model.RedirectUrl != null)
            {
                var content = @"<!DOCTYPE html>
                <html>
                    <head>
                        <meta http-equiv='refresh' content='0;{0}'>
                    </head>
                    <body></body>
                </html>";

                var formattedContent = string.Format(content, model.RedirectUrl);

                return Task.FromResult(Extensions.StringToStream(formattedContent));
            }

            return Render(model, "loggedOut");
        }
    }
}