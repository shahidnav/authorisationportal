﻿using System;
using System.Security.Claims;
using System.Threading.Tasks;
using Im.IdentityManager.AspNetIdentity;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security;

namespace Im.IdentityManager.UserAdmin
{
    public class ApplicationRoleManager : RoleManager<IdentityRole>
    {
        public ApplicationRoleManager(RoleStore<IdentityRole> roleStore)
            : base(roleStore)
        { }
        }

    // Configure the application sign-in manager which is used in this application.
    public class ApplicationSignInManager : SignInManager<ApplicationUser, string>
    {
        public ApplicationSignInManager(MultitenantUserManager userManager, IAuthenticationManager authenticationManager)
            : base(userManager, authenticationManager)
        { }

        protected internal MultitenantUserManager MultiTenantUserManager => UserManager as MultitenantUserManager;

        public string TenantId
        {
            get { return MultiTenantUserManager?.TenantId; }
            set
            {
                if (MultiTenantUserManager == null)
            {
                    throw new ApplicationException(
                        "Multitenant operation failed due to invalid user manager, please check user manager construction.");
    }

                MultiTenantUserManager.TenantId = value;
    }
        }

        public override Task<ClaimsIdentity> CreateUserIdentityAsync(ApplicationUser user)
        {
            return user.GenerateUserIdentityAsync((MultitenantUserManager)UserManager);
        }

        public static ApplicationSignInManager Create(IdentityFactoryOptions<ApplicationSignInManager> options, IOwinContext context)
        {
            return new ApplicationSignInManager(context.GetUserManager<MultitenantUserManager>(), context.Authentication);
        }

        public override async Task<SignInStatus> PasswordSignInAsync(string userName, string password, bool isPersistent, bool shouldLockout)
        {
            var user = await UserManager.FindByNameAsync(userName);
            if (user == null)
            {
                return SignInStatus.Failure;
            }

            var verifyPasswordResult = UserManager.PasswordHasher.VerifyHashedPassword(user.PasswordHash, password);

            if(verifyPasswordResult == PasswordVerificationResult.Failed)
                verifyPasswordResult = UserManager.PasswordHasher.VerifyHashedPassword(user.PasswordHash, userName + password);

            switch (verifyPasswordResult)
            {
                case PasswordVerificationResult.Success:
                    return await base.PasswordSignInAsync(userName, password, isPersistent, shouldLockout);

                case PasswordVerificationResult.SuccessRehashNeeded:
                    user.PasswordHash = UserManager.PasswordHasher.HashPassword(password);
                    await UserManager.UpdateAsync(user);
                    return await base.PasswordSignInAsync(userName, password, isPersistent, shouldLockout);

                case PasswordVerificationResult.Failed:
                default:
                    return SignInStatus.Failure;
            }
        }
    }
}
