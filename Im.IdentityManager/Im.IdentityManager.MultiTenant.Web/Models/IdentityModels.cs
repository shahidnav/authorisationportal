﻿using System.Configuration;
using System.Data.Entity;
using AspNet.Identity.EntityFramework.Multitenant;
using IdentityManager;
using IdentityManager.AspNetIdentity;
using IdentityManager.Configuration;
using Im.IdentityManager.AspNetIdentity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Im.IdentityManager.UserAdmin.Models
{
    public class ApplicationIdentityManagerService : AspNetIdentityManagerService<ApplicationUser, string, IdentityRole, string>
    {
        public ApplicationIdentityManagerService(MultitenantUserManager userMgr, ApplicationRoleManager roleMgr)
            : base(userMgr, roleMgr)
        {
            userMgr.TenantId = ConfigurationManager.AppSettings["TenantId"];
        }
    }

    public class ApplicationIdentityManagerServiceFactory
    {
        public IdentityManagerServiceFactory CreateFactory()
        {
            var factory = new IdentityManagerServiceFactory
            {
                IdentityManagerService = new Registration<IIdentityManagerService, ApplicationIdentityManagerService>()
            };

            factory.Register(new Registration<MultitenantUserManager>());
            factory.Register(new Registration<MultitenantUserStore<ApplicationUser>>());
            factory.Register(new Registration<ApplicationRoleManager>());
            factory.Register(new Registration<RoleStore<IdentityRole>>());
            factory.Register(new Registration<DbContext>(typeof(ApplicationDbContext)));

            return factory;
        }
    }
}