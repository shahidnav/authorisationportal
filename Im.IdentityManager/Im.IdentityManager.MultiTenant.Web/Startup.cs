﻿using IdentityManager.Configuration;
using Im.IdentityManager.UserAdmin;
using Im.IdentityManager.UserAdmin.Models;
using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(Startup))]
namespace Im.IdentityManager.UserAdmin
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            
            app.Map("/identitymanager", idm =>
            {
                idm.UseIdentityManager(new IdentityManagerOptions
                {
                    Factory = new ApplicationIdentityManagerServiceFactory().CreateFactory()
                });
            });
        }
    }
}
