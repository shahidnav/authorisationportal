﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using IdentityManager;
using IdentityManager.Configuration;
using AspNet.Identity.EntityFramework.Multitenant;
using IdentityManager.AspNetIdentity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin;


using Owin;

[assembly: OwinStartup(typeof(Im.IdentityManager.Web.Startup))]

namespace Im.IdentityManager.Web
{
    public class DisposableAspNetIdentityManagerService : IDisposable, IIdentityManagerService
    {
        private IIdentityManagerService decorated;
        private MultitenantIdentityDbContext<MultitenantIdentityUser> dbContext;
        public Task<IdentityManagerMetadata> GetMetadataAsync()
        {
            return decorated.GetMetadataAsync();
        }

        public Task<IdentityManagerResult<CreateResult>> CreateUserAsync(IEnumerable<PropertyValue> properties)
        {
            return decorated.CreateUserAsync(properties);
        }

        public Task<IdentityManagerResult> DeleteUserAsync(string subject)
        {
            return decorated.DeleteUserAsync(subject);
        }

        public Task<IdentityManagerResult<QueryResult<UserSummary>>> QueryUsersAsync(string filter, int start, int count)
        {
            return decorated.QueryUsersAsync(filter, start, count);
        }

        public Task<IdentityManagerResult<UserDetail>> GetUserAsync(string subject)
        {
            return decorated.GetUserAsync(subject);
        }

        public Task<IdentityManagerResult> SetUserPropertyAsync(string subject, string type, string value)
        {
            return decorated.SetUserPropertyAsync(subject, type, value);
        }

        public Task<IdentityManagerResult> AddUserClaimAsync(string subject, string type, string value)
        {
            return decorated.AddUserClaimAsync(subject, type, value);
        }

        public Task<IdentityManagerResult> RemoveUserClaimAsync(string subject, string type, string value)
        {
            return decorated.RemoveUserClaimAsync(subject, type, value);
        }

        public Task<IdentityManagerResult<CreateResult>> CreateRoleAsync(IEnumerable<PropertyValue> properties)
        {
            return decorated.CreateRoleAsync(properties);
        }

        public Task<IdentityManagerResult> DeleteRoleAsync(string subject)
        {
            return decorated.DeleteRoleAsync(subject);
        }

        public Task<IdentityManagerResult<QueryResult<RoleSummary>>> QueryRolesAsync(string filter, int start, int count)
        {
            return decorated.QueryRolesAsync(filter, start, count);
        }

        public Task<IdentityManagerResult<RoleDetail>> GetRoleAsync(string subject)
        {
            return decorated.GetRoleAsync(subject);
        }

        public Task<IdentityManagerResult> SetRolePropertyAsync(string subject, string type, string value)
        {
            return decorated.SetRolePropertyAsync(subject, type, value);
        }

        public DisposableAspNetIdentityManagerService(IIdentityManagerService decorated, MultitenantIdentityDbContext<MultitenantIdentityUser> dbContext)
        {
            this.decorated = decorated;
            this.dbContext = dbContext;
        }

        public void Dispose()
        {
            this.dbContext.Dispose();
        }
    }

    public class AspNetIdentityIdentityManagerFactory
    {
        readonly string connString;

        public AspNetIdentityIdentityManagerFactory(string connString)
        {
            this.connString = connString;
        }

        public IIdentityManagerService Create()
        {
            var db = new MultitenantIdentityDbContext<MultitenantIdentityUser>(connString);
            var userStore = new MultitenantUserStore<MultitenantIdentityUser>(db);
            var userMgr = new Microsoft.AspNet.Identity.UserManager<MultitenantIdentityUser>(userStore);
            var roleStore = new RoleStore<IdentityRole>(db);
            var roleMgr = new Microsoft.AspNet.Identity.RoleManager<IdentityRole>(roleStore);

            var svc = new AspNetIdentityManagerService<MultitenantIdentityUser, string, IdentityRole, string>(userMgr, roleMgr);

            return new DisposableAspNetIdentityManagerService(svc, db);
        }
    }

    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            // For more information on how to configure your application, visit http://go.microsoft.com/fwlink/?LinkID=316888
            var options = new IdentityManagerOptions
            {
                Factory = new IdentityManagerServiceFactory()
                // configuration values in here
            };
            
            app.UseIdentityManager(options);
        }
    }
}
